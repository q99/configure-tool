import sys

import configuration_tools


class Database:
    Host = "example.com"
    Port = 10001
    DatabaseName = "db"
    User = "admin"
    Password = "nimda"


for title, res in configuration_tools.from_file(Database):
    setattr(sys.modules[__name__], title, res)
