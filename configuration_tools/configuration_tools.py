import sys
from pathlib import Path
from runpy import run_path
from typing import Union

__default__ = "default"


def _get_difference_attributes(old_obj, new_obj) -> Union[tuple, None]:
    diff = []

    for attr in filter(lambda name: not name.startswith("__"), old_obj.__dict__):

        if getattr(new_obj, attr) is None:
            diff.append(attr)

    return tuple(diff) if diff else None


def _parse_file(file: Union[str, Path, int]) -> Path:
    if isinstance(file, str):

        key = "{}{}".format("--" if len(file) > 1 else "-", file)

        if key not in sys.argv:
            raise AssertionError(f"Can't find key {key}")

        file = sys.argv[1 + sys.argv.index(key)]

        if file == __default__:
            raise AssertionError("Default value configure.")

        file = Path(file)

    elif isinstance(file, int):
        if len(sys.argv) < file:
            raise AssertionError(f"Can't find argument for number {file}")

        file = Path(sys.argv[file])

    elif isinstance(file, Path):
        file = file

    else:
        raise TypeError("Can't parse argument {}".format(file))

    return _check_path(file)


def _check_path(path) -> Path:
    if not path.exists():
        raise ValueError(f"Can't find configuration_tools file by path {str(path)}")

    if path.suffix != '.py':
        raise TypeError(f"Can't parse configuration_tools file. file suffix != .py")

    return path


def from_file(*configure_structs, configuration_file: Union[Path, int, str]):
    try:
        path = _parse_file(configuration_file)

    except AssertionError:
        return ()

    configure_structs = {conf.__name__: conf for conf in configure_structs}

    for title, configure in filter(lambda obj: obj[0] in configure_structs.keys(), run_path(path).items()):

        difference = _get_difference_attributes(configure_structs[title], configure)

        if difference is None:
            yield title, configure

        raise AssertionError(f"Can't parse {str(path)}. Not found in {title} attributes: {difference}. ")
